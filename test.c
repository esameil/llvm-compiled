#include <limits.h>

int foo(int a) {
	  return a;
}

int direct_version() {
	  int i, b = 0;
	    for (i = 0; i < INT_MAX; ++i) {
		          b = foo(b);
			    }
	      return b;
}

int indirect_version(int (*fn)(int)) {
	  int i, b = 0;
	    
	    for (i = 0; i < INT_MAX; ++i) {
		        b = fn(b);
			  }

	      return b;
}

int main(int argc, char *argv[]) {
	  if (argc == 2 && argv[1][0] == 'd') {
		      return direct_version();
		        }
	    else {
		        return indirect_version(&foo);
			  }
}


